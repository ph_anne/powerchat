var express = require('express');
var bodyParser = require('body-parser');

var app = express();
var port = process.env.PORT || 3003;

var id = 1;
var items = [];

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.route('/api/chat/:id')
    .get(function(req, res) {

        try {
            var id = req.params.id;

            var r = items.filter(a => a.id > id);

            res.json(r);
            
        } catch(e) {
            console.log(e);
            res.status(500).send('Server error');
        }
    });
    
app.route('/api/chat')
    .post(function(req, res) {

        try {
        
        var msg = req.body.message|| 'nothing';
        var name = req.body.name || 'unknown';
        items.push({
            name: name,
            message: msg,
            id: id++,
            timestamp: new Date(),
        });
        console.log(`added message "${msg}" from "${name}"`);
        res.status(200).send('OK');
        }
        catch(e) {
            console.log(e);
            res.status(500).send('Server error');
        }
    });

app.listen(port);
console.log(`PowerChat startet on ${port}`);